# SmartMaps

## Description

SmartMaps is a vue component app that load a list os companies in json format with a huge data. Inside this data exists longitude and latitude, and SmartMaps propose is read this coordinates to show on Google Maps api the positions with icons.

Using the other data got in companies' json, we apply some filters to select by companies' sector, companies' tags and companies' category.


## Installation
The project use yarn and to install all dependencies need run:
```bash
npm install
```

## Usage
This moment the project need login. You can use this credentials to access:
```bash
login: user3@speedio.com.br
password: gkfxeszy7oxc
```

## Roadmap
For this moment this is our propose:

"I will send you access to my repo (gitlab) and a sample task I would like you to do it (it should take 30min. I am looking for a long term commitment). If you can't work on the sample task explain me how would you do it.

1 - Check if company has coordinates (long and lat);
      Implement a validation to check if company has lat and long in your data. If not, dont show in the select list

2 - Show message and a counter with number of companies dont have coordinates
      If info_set has companies with no coordinates (lat and long), remove those before the construction business selector and pass the message with a counter of how many companies will not be shown. Just like the image here:  https://imgur.com/a/sHRfvuT"


## Contributing
Please, commit your code in a branch with your gitlab id or email without domain. This practice turn easier the select
