import Vue from 'vue'
import App from './App'
import router from './router'
import {store} from './store/store'
import axios from 'axios'
import * as VueGoogleMaps from "vue2-google-maps";

import '@/assets/css/grid.min.css'
import '@/assets/css/main.css'
import '@/assets/css/fontawesome.min.css'

Vue.config.productionTip = false;
Vue.prototype.$http = axios;

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyAOHSMMhB3tSoIZuf8eRqQBeJbSl0CrfUw",
        libraries: "places" // necessary for places input
    }
});


new Vue({
    el: '#app',
    router,
    store:store,
    components: {App},
    template: '<App/>'
});
