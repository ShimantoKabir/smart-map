import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

const LoginReg = () => import('@/pages/LoginReg');
const GoogleMap = () => import('@/views/GoogleMap');

export default new Router({
    routes: [
        {
            path: '/',
            name: 'LoginReg',
            component: LoginReg,
            meta:{
                requiresAuth: false
            }
        },
        {
            path: '/google-map',
            name: 'GoogleMap',
            component: GoogleMap,
            meta:{
                requiresAuth: false
            }
        }
    ],
    mode: 'hash'
})


